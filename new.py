#!/usr/bin/python
# -*- coding: utf-8 -*-
# @Author: June
# @Time :2018/9/20
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi
import json
import schedule
import time
import operator
from retrying import retry
import logging
logging.basicConfig(level=logging.INFO,
filename='./log.txt',
filemode='a',
format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(funcName)s: %(message)s')
region = 'ap-chengdu'  # 区域代码
clb_num = 2  # 一次创建几个负载均衡实例
sche_time = 1  # 周期时间
secretId_clb = 'AKID2ANa8hZjEjDBLS6dgmqr5SrtuDUk5mO0'  # 创建负载均衡的账号
secretKey_clb = 'wqKmBZL8530u8Nbr6IUemHlS82vmevEX'
secretId_ana = 'AKID2ANa8hZjEjDBLS6dgmqr5SrtuDUk5mO0'  # 进行云解析的账号
secretKey_ana = 'wqKmBZL8530u8Nbr6IUemHlS82vmevEX'
clb_ins = 'ins-1vf2872h'  # 云服务器的id
head = 'test'  # 解析的域名头
domain = 'jiashu2018.space'  # 域名

config_clb = {
            'Region': region,
            'secretId': secretId_clb,
            'secretKey': secretKey_clb,
            'method': 'post'
}
config_ana = {
    'Region': region,
    'secretId': secretId_ana,
    'secretKey': secretKey_ana,
    'method': 'post'
}
params_create_clb = {
    'loadBalancerType': 2,
    'forward': 0,
    'number': clb_num
}
service_clb = QcloudApi(module='lb', config=config_clb, region=region)
service_ana = QcloudApi(module='cns', config=config_ana, region=region)


def judge(result):
    return result is None


@retry(retry_on_result=judge)
def task(task_id):
    res = json.loads(service_clb.call('DescribeLoadBalancersTaskResult', {'requestId': task_id}))
    print "query", task_id, res
    logging.info(str(task_id)+'|'+str(res))
    return res['data']['status'] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def create_balances():
    """创建一个或多个负载均衡实例"""
    create_balances_res = json.loads(service_clb.call('CreateLoadBalancer', params_create_clb))
    print create_balances_res
    try:
        logging.info(str(create_balances_res))
    except Exception as e:
        print e
    print '1111'
    print '2222'
    return create_balances_res['unLoadBalancerIds'].values()[0] if create_balances_res['code'] == 0 else None


@retry(retry_on_result=judge)
def create_listener(clb_id):
    """为负载均衡实例创建监听器"""
    params_listener = {
        'loadBalancerId': clb_id.encode(),
        'listeners.0.listenerName': '80',
        'listeners.0.protocol': 2,
        'listeners.0.loadBalancerPort': 80,
        'listeners.0.instancePort': 80,
        'listeners.0.healthSwitch': 0
    }
    create_listener_res = json.loads(service_clb.call('CreateLoadBalancerListeners', params_listener))
    print "为负载均衡实例创建监听器"
    print create_listener_res
    logging.info(str(create_listener_res))
    return create_listener_res['requestId'] if create_listener_res['code'] == 0 else None


@retry(retry_on_result=judge)
def balances_to_cvm(clb_id):
    """负载均衡绑定"""
    params_listener_blind = {
        'loadBalancerId': clb_id.encode(),
        'backends.0.instanceId': clb_ins,
    }
    res = json.loads(service_clb.call('RegisterInstancesWithLoadBalancer', params_listener_blind))
    print "负载均衡绑定"
    print res
    logging.info(str(res))
    return res['requestId'] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def record_list():
    """获取解析记录列表"""
    params_ym_list = {
        'domain': domain,
        'subDomain': head
    }
    res = json.loads(service_ana.call('RecordList', params_ym_list))
    print "获取解析记录列表"
    print res
    logging.info(str(res))
    return res['data']['records'] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def delete_record(ym_id):
    """删除旧的解析记录"""
    params_delete_ym = {
        'domain': domain,
        'recordId': ym_id
    }
    res = json.loads(service_ana.call('RecordDelete', params_delete_ym))
    print "删除旧的解析记录"
    print res
    logging.info(str(res))
    return res['codeDesc'] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def value_to_ip(ym_list_value):
    """解析记录的ip查clb的id"""
    params_clb_list_byip = {
        'forward': 0,
        'loadBalancerType': 2
    }
    for j, value in enumerate(ym_list_value):
        params_clb_list_byip['loadBalancerVips.%d' % j] = value
    res = json.loads(service_clb.call('DescribeLoadBalancers', params_clb_list_byip))
    print "解析记录的ip查clb的id"
    print res
    logging.info(str(res))
    return [query_id['loadBalancerId'] for query_id in res['loadBalancerSet']] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def create_clb_vip(clb_list):
    """查询新创建的clb的vip"""
    params_clb_list_byid = {
        'forward': 0,
        'loadBalancerType': 2
    }
    for k, new_clb_id in enumerate(clb_list):
        params_clb_list_byid['loadBalancerIds.%d' % k] = new_clb_id
    res = json.loads(service_clb.call('DescribeLoadBalancers', params_clb_list_byid))
    print "查询新创建的clb的vip"
    print res
    logging.info(str(res))
    return [query_ip['loadBalancerVips'][0] for query_ip in res['loadBalancerSet']] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def delete_clb(clb_ids):
    """删除clb"""
    params_delete_clb = {}
    for i, del_id in enumerate(clb_ids):
        params_delete_clb['loadBalancerIds.%d' % i] = del_id.encode()
    res = json.loads(service_clb.call('DeleteLoadBalancers', params_delete_clb))
    print "删除clb"
    print res
    logging.info(str(clb_ids))
    logging.info(str(res))
    return res['requestId'] if res['code'] == 0 else None


@retry(retry_on_result=judge)
def create_record(create_ana_ip):
    """生成新的解析记录"""
    params_ana_create = {
        'domain': domain,
        'subDomain': head,
        'recordType': 'A',
        'recordLine': '默认',
        'value': create_ana_ip.encode()
    }
    res = json.loads(service_ana.call('RecordCreate', params_ana_create))
    print "生成新的解析记录"
    print res
    logging.info(str(create_ana_ip))
    logging.info(str(res))
    return res['data']['record']['status'] if res['code'] == 0 else None


def main():
    clb_list = create_balances()
    print 'clb_list', clb_list
    logging.info('创建clb实例'+str(clb_list))
    for clb_id in clb_list:
        create_listener_task = create_listener(clb_id)
        while True:
            if task(create_listener_task) == 0:
                break
            else:
                time.sleep(3)
        listener_blind_task = balances_to_cvm(clb_id)
        while True:
            if task(listener_blind_task) == 0:
                break
            else:
                time.sleep(3)
    ym_list_rest = record_list()
    for ki in ym_list_rest:
        ki['updated_int'] = time.mktime(
            time.strptime(ki['updated_on'].encode('raw_unicode_escape'), "%Y-%m-%d %H:%M:%S"))
    ym_list_res = sorted(ym_list_rest, key=operator.itemgetter('updated_int'))[:clb_num]
    print 'ym_list_res', ym_list_res
    ym_list_id = [ids['id'] for ids in ym_list_res]
    ym_list_value = [ids['value'] for ids in ym_list_res]
    print 'ym_list_id', ym_list_id
    logging.info('旧的记录id' + str(ym_list_id))
    print 'ym_list_value', ym_list_value
    logging.info('旧的记录vip' + str(ym_list_value))
    for ym_id in ym_list_id:
        delete_record(ym_id)
    if ym_list_value:
        clb_ids = value_to_ip(ym_list_value)
    else:
        clb_ids = []
    print 'clb_ids', clb_ids
    logging.info('删除clb的id'+ str(clb_ids))
    clb_ips = create_clb_vip(clb_list)
    print 'clb_ips', clb_ips
    logging.info('新增的绑定vip' + str(clb_ips))
    if clb_ids:
        delete_clb(clb_ids)
    for create_ana_ip in clb_ips:
        create_record(create_ana_ip)
        time.sleep(3)
if __name__ == '__main__':
    main()
    schedule.every(sche_time).minutes.do(main)
    next_time = ''
    while True:
        schedule.run_pending()
        time.sleep(5)
        if schedule.next_run() != next_time:
            print schedule.next_run()
            next_time = schedule.next_run()
            logging.info('下次任务执行时间'+str(next_time))