#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi

action = 'CreateLoadBalancer'
region = 'ap-chengdu'
params = {
    'loadBalancerType': 2,
    'forward': 0
}
try:
    service = QcloudApi(region=region)
    print service.call(action, params)
except Exception, e:
    print 'exception:', e