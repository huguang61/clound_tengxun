#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi

action = 'RecordDelete'
region = 'ap-chengdu'
params = {
    'domain': 'jiashu2018.space',
    'recordId': 381066868
}
try:
    service = QcloudApi(module='cns', region=region)
    print service.generateUrl(action, params)
    print service.call(action, params)
except Exception, e:
    print 'exception:', e