#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi
import json

action = 'DescribeLoadBalancerListeners'  # 查询负载均衡的监听器

"""
loadBalancerId	必传 负载均衡实例 ID
listenerIds.n	非必传 负载均衡监听器ID。
protocol	    非必传 监听器协议类型 1：HTTP，2：TCP，3：UDP，4：HTTPS。
loadBalancerPort 非必传	负载均衡监听器端口。
status	非必传	负载均衡监听器的状态，当输入负载均衡监听器ID来查询时，忽略该字段。
"""
region = 'ap-chengdu'
params = {
    'loadBalancerId': "lb-gx6tmw87",

    # 'listenerIds.1':"lbl-ne2v3kqw",
    # 'protocol':4,
    # 'loadBalancerPort':2558,
}

try:
    service = QcloudApi(region='ap-chengdu')
    print service.generateUrl(action, params)
    listeners = json.loads(service.call(action, params))['listenerSet']

    for lis in listeners:
        print lis['unListenerId']

except Exception, e:
    print 'exception:', e
