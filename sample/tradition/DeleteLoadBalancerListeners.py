#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi

action = 'DeleteLoadBalancerListeners'

"""
loadBalancerId--yes
listenerIds.n---yes
"""

region = 'ap-chengdu'
params = {
    'loadBalancerId': "lb-gx6tmw87",
    'listenerIds.0': "lbl-glec7k3t",
}

try:
    service = QcloudApi(region=region)
    print service.generateUrl(action, params)
    print service.call(action, params)
except Exception, e:
    print 'exception:', e
