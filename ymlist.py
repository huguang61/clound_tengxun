#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi
import json

action = 'RecordList'
region = 'ap-chengdu'
params = {
    'domain': 'jiashu2018.space',
    'subDomain': 'test'
}
try:
    service = QcloudApi(module='cns', region=region)
    print service.generateUrl(action, params)
    res = json.loads(service.call(action, params))
    pre_id = res['data']['records']
    for i in pre_id:
        print i['updated_on']
except Exception, e:
    print 'exception:', e