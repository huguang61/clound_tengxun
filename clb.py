#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import os
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)) + "/../..")
from src.QcloudApi.qcloudapi import QcloudApi
import json
import schedule
import time
import operator
import logging
logging.basicConfig(level=logging.INFO,
filename='./log.txt',
filemode='w',
format='%(asctime)s - %(filename)s[line:%(lineno)d] - %(levelname)s: %(message)s')


region = 'ap-chengdu'  # 区域代码
clb_num = 2  # 一次创建几个负载均衡实例
sche_time = 2  # 周期时间
secretId_clb = ''  # 创建负载均衡的账号
secretKey_clb = ''
secretId_ana = ''  # 进行云解析的账号
secretKey_ana = ''
clb_ins = ''  # 云服务器的id
head = 'test'  # 解析的域名头
domain = ''  # 域名


if __name__ == '__main__':
    def foo():
        config_clb = {
            'Region': region,
            'secretId': secretId_clb,
            'secretKey': secretKey_clb,
            'method': 'post'
        }
        config_ana = {
            'Region': region,
            'secretId': secretId_ana,
            'secretKey': secretKey_ana,
            'method': 'post'
        }
        params_create_clb = {
            'loadBalancerType': 2,
            'forward': 0,
            'number': clb_num
        }
        service_clb = QcloudApi(module='lb', config=config_clb, region=region)
        service_ana = QcloudApi(module='cns', config=config_ana, region=region)
        """
        创建一个或多个负载均衡实例
        """
        create_clb_mess = json.loads(service_clb.call('CreateLoadBalancer', params_create_clb))
        if create_clb_mess['code'] != 0:
            logging.info(create_clb_mess['message'])
        clb_list = create_clb_mess['unLoadBalancerIds'].values()[0]
        logging.info('创建了负载均衡实例', clb_list)
        """
        为负载均衡实例创建监听器
        """
        time.sleep(5)
        for clb_id in clb_list:
            params_listener = {
                'loadBalancerId': clb_id.encode(),
                'listeners.0.listenerName': '80',
                'listeners.0.protocol': 2,
                'listeners.0.loadBalancerPort': 80,
                'listeners.0.instancePort': 80,
                'listeners.0.healthSwitch': 0
            }
            create_clb_mess = json.loads(service_clb.call('CreateLoadBalancerListeners', params_listener))
            logging.info('创建监听器成功', create_clb_mess)
        """
        负载均衡实例绑定本机
        """
        time.sleep(5)
        for clb_id in clb_list:
            logging.info(clb_id.encode(), type(clb_id.encode()))
            params_listener_blind = {
                'loadBalancerId': clb_id.encode(),
                'backends.0.instanceId': clb_ins,
            }
            clb_blind_mess = json.loads(service_clb.call('RegisterInstancesWithLoadBalancer', params_listener_blind))
            logging.info('负载均衡绑定成功', clb_blind_mess)
        """
        查询名叫domain的云解析下面的域名解析记录的负载均衡实例的VIP
        这里每次只选择出两个，相当于新建两个，再删除两个
        """
        time.sleep(10)
        params_ym_list = {
            'domain': domain,
            'subDomain': head
        }
        ym_list_rest = json.loads(service_ana.call('RecordList', params_ym_list))['data']['records']
        logging.info('创建云解析成功', ym_list_rest)
        for ki in ym_list_rest:
            ki['updated_int'] = time.mktime(time.strptime(ki['updated_on'].encode('raw_unicode_escape'), "%Y-%m-%d %H:%M:%S"))
        ym_list_res = sorted(ym_list_rest, key=operator.itemgetter('updated_int'))[:clb_num]
        ym_list_id = [ids['id'] for ids in ym_list_res]
        ym_list_value = [ids['value'] for ids in ym_list_res]
        logging.info('早的解析记录', ym_list_id, ym_list_value)
        """
        将上面那个函数查找出来的两个记录在云解析中给删除
        """
        time.sleep(5)
        for ym_id in ym_list_id:
            params_delete_ym = {
                'domain': domain,
                'recordId': ym_id
            }
            ana_del_res = service_ana.call('RecordDelete', params_delete_ym)
            logging.info('删除记录成功', ana_del_res)
        """
        根据上面的记录查询出来负载实例的ID
        """
        time.sleep(5)
        params_clb_list_byip = {
            'forward': 0,
            'loadBalancerType': 2
        }
        for j, value in enumerate(ym_list_value):
            params_clb_list_byip['loadBalancerVips.%d' % j] = value

        clb_id_res = json.loads(service_clb.call('DescribeLoadBalancers', params_clb_list_byip))
        clb_ids = [query_id['loadBalancerId'] for query_id in clb_id_res['loadBalancerSet']]
        logging.info('删除解析的ip对应clb的id', clb_ids)
        """
        查询新建立的负载均衡的实例的VIP
        """
        time.sleep(5)
        params_clb_list_byid = {
            'forward': 0,
            'loadBalancerType': 2
        }
        for k, new_clb_id in enumerate(clb_list):
            params_clb_list_byid['loadBalancerIds.%d' % k] = new_clb_id

        clb_ip_res = json.loads(service_clb.call('DescribeLoadBalancers', params_clb_list_byid))
        clb_ips = [query_ip['loadBalancerVips'][0] for query_ip in clb_ip_res['loadBalancerSet']]
        logging.info('查询新创建的clb的ip', clb_ips)
        """
        根据上面查找的结果，将对应的负载均衡实例给删除掉
        """
        time.sleep(5)
        params_delete_clb = {}
        for i, del_id in enumerate(clb_ids):
            params_delete_clb['loadBalancerIds.%d' % i] = del_id.encode()
        service_clb.call('DeleteLoadBalancers', params_delete_clb)
        logging.info('删除clb')
        """
        创建名叫domain云解析下面的新建的负载均衡实例的解析记录
        """
        time.sleep(10)
        for create_ana_ip in clb_ips:
            params_ana_create = {
                'domain': domain,
                'subDomain': head,
                'recordType': 'A',
                'recordLine': '默认',
                'value': create_ana_ip.encode()
            }
            service_ana.call('RecordCreate', params_ana_create)
            logging.info('创建解析记录')
            time.sleep(6)
    schedule.every(sche_time).minutes.do(foo)

    while True:
        schedule.run_pending()
        time.sleep(5)



